<?php

declare(strict_types=1);

use CarMaster\Classes\AutoServiceOrder;
use CarMaster\Classes\Customer;
use CarMaster\Classes\Manager;
use CarMaster\Classes\Mechanic;
use CarMaster\Classes\Vehicle;

use CarMaster\Exceptions\CustomerValidationException;
use CarMaster\Exceptions\EmployeeValidationException;
use CarMaster\Exceptions\VehicleValidationException;
use CarMaster\Command\CreateOrderCommand;

use Symfony\Component\Console\Application;

require_once __DIR__ . '/vendor/autoload.php';

try {
  $application = new Application();
  $application->add(new CreateOrderCommand());
  $application->run();
} catch (EmployeeValidationException $exception) {
  echo $exception->getMessage() . PHP_EOL;
} catch (CustomerValidationException $exception) {
  echo $exception->getMessage() . PHP_EOL;
} catch (VehicleValidationException $exception) {
  echo $exception->getMessage() . PHP_EOL;
} catch (Exception $e) {
  echo $e->getMessage() . PHP_EOL;
}

//try {
//  $mechanic = new Mechanic('Anton', 0.2);
//  $manager = new Manager('Oleh', 2000);
//
//  $vehicle = new Vehicle('renault', '1234qwert');
//
//  $customer = new Customer('Serhii', 'Vozniuk', '+380631234566', $vehicle);
//
//  $serviceOrder = new AutoServiceOrder($customer, $mechanic);
//  $serviceOrder->setServices('repair engine', 4000);
//  $serviceOrder->setServices('replacement of brake pads', 300);
//  $serviceOrder->calculateTotalCost();
//  var_dump($serviceOrder->getTotalCost());
//  var_dump($serviceOrder);
//} catch (EmployeeValidationException $exception) {
//  echo $exception->getMessage() . PHP_EOL;
//} catch (CustomerValidationException $exception) {
//  echo $exception->getMessage() . PHP_EOL;
//} catch (VehicleValidationException $exception) {
//  echo $exception->getMessage() . PHP_EOL;
//}
