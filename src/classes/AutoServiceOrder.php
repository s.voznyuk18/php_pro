<?php

declare(strict_types=1);

namespace CarMaster\Classes;

class AutoServiceOrder
{
    private Customer $customer;
    private Mechanic $specialist;
    private array $services;
    private float $totalCost = 0;

    public function __construct(Customer $customer, Mechanic $specialist)
    {
        $this->setCustomer($customer);
        $this->setSpecialist($specialist);
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }

    public function getSpecialist(): Mechanic
    {
        return $this->specialist;
    }

    public function setSpecialist(Mechanic $specialist): void
    {
        $this->specialist = $specialist;
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function setServices(string $serviceTitle, float $serviceCost): void
    {
        $this->services[$serviceTitle] = $serviceCost;
    }

    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    public function calculateTotalCost(): void
    {
        if (count($this->services) < 1) {
            $this->totalCost = 0;
        } else {
            foreach ($this->services as $service => $cost) {
                $this->totalCost += $cost;
            }
        }
    }
}