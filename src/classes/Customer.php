<?php

declare(strict_types=1);

namespace CarMaster\Classes;

use CarMaster\Exceptions\CustomerValidationException;

class Customer
{
    private string $firstName;
    private string $lastName;
    private string $phone;
    private Vehicle $vehicle;

    public function __construct(string $firstName, string $lastName, string $phone, Vehicle $vehicle)
    {
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->setPhone($phone);
        $this->setVehicle($vehicle);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): void
    {
        if (empty($firstName)) {
            throw new CustomerValidationException($this, 'firstName is required');
        }
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): void
    {
        if (empty($lastName)) {
            throw new CustomerValidationException($this, 'lastName is required');
        }
        $this->lastName = $lastName;
    }

    public function getFullName(): string
    {
        return "$this->firstName $this->lastName";
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        if (empty($phone)) {
            throw new CustomerValidationException($this, 'phone is required');
        }
        $this->phone = $phone;
    }

    public function getVehicle(): Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(Vehicle $vehicle): void
    {
        $this->vehicle = $vehicle;
    }
}