<?php

declare(strict_types=1);

namespace CarMaster\Classes;

use CarMaster\Exceptions\EmployeeValidationException;

abstract class Employee
{
    private string $name;
    protected float $salary;

    public function __construct(string $name)
    {
        $this->setName($name);
    }

    abstract public function calculateSalary(float $baseSalary);

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        if (empty($name)) {
            throw new EmployeeValidationException($this, 'name is required');
        }
        $this->name = $name;
    }
}