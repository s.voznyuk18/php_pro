<?php

declare(strict_types=1);

namespace CarMaster\Classes;

class Manager extends Employee
{
    const POSITION = 'manager';
    private int $bonus;

    public function __construct(string $name, int $bonus)
    {
        parent::__construct($name);
        $this->setBonus($bonus);
    }

    public function getBonus(): int
    {
        return $this->bonus;
    }

    public function setBonus(int $bonus): void
    {
        $this->bonus = $bonus;
    }

    public function calculateSalary(float $baseSalary): void
    {
        $this->salary = $baseSalary + $this->bonus;
    }
}