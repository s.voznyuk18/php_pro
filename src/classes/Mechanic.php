<?php

declare(strict_types=1);

namespace CarMaster\Classes;

class Mechanic extends Employee
{
    const POSITION = 'mechanic';
    private float $coefficient;

    public function __construct(string $name, float $coefficient)
    {
        parent::__construct($name);
        $this->setCoefficient($coefficient);
    }

    public function getCoefficient(): float
    {
        return $this->coefficient;
    }

    public function setCoefficient(float $coefficient): void
    {
        $this->coefficient = $coefficient;
    }

    public function calculateSalary(float $baseSalary): void
    {
        $this->salary = $baseSalary * $this->coefficient;
    }
}