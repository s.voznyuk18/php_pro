<?php

namespace CarMaster\Classes;

use CarMaster\Exceptions\VehicleValidationException;

class Vehicle
{
    private string $model;
    private string $vinCode;

    public function __construct(string $model, string $vinCode)
    {
        $this->setModel($model);
        $this->setVinCode($vinCode);
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function setModel(string $model): void
    {
        if (empty($model)) {
            throw new VehicleValidationException($this, 'model is required');
        }
        $this->model = $model;
    }

    public function getVinCode(): string
    {
        return $this->vinCode;
    }

    public function setVinCode(string $vinCode): void
    {
        if (empty($vinCode)) {
            throw new VehicleValidationException($this, 'vinCode is required');
        }
        $this->vinCode = $vinCode;
    }
}