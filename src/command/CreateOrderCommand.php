<?php

declare(strict_types=1);

namespace CarMaster\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Helper\Table;

use CarMaster\Classes\AutoServiceOrder;
use CarMaster\Classes\Customer;
use CarMaster\Classes\Mechanic;
use CarMaster\Classes\Vehicle;

#[AsCommand(
    name: 'app:create-order',
    description: 'Creates a new order.',
    aliases: ['app:create-order'],
    hidden: false
)]
class CreateOrderCommand extends Command
{
    private array $dataTable;

    protected function configure(): void
    {
        $this
            ->addArgument('firstName', InputArgument::REQUIRED, "Customer's first name")
            ->addArgument('lastName', InputArgument::REQUIRED, "Customer's last name")
            ->addArgument('phone', InputArgument::REQUIRED, "Customer's phone")
            ->addArgument('vehicleModel', InputArgument::REQUIRED, "Vehicle's model")
            ->addArgument('vinCode', InputArgument::REQUIRED, "Vehicle's vinCode")
            ->addArgument(
                'services',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'List of services and their cost'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mechanic = new Mechanic('Anton', 0.2);

        $vehicle = new Vehicle($input->getArgument('vehicleModel'), $input->getArgument('vinCode'));

        $customer = new Customer(
            $input->getArgument('firstName'),
            $input->getArgument('lastName'),
            $input->getArgument('phone'),
            $vehicle
        );

        $serviceOrder = new AutoServiceOrder($customer, $mechanic);

        $services = $input->getArgument('services');
        foreach ($services as $service) {
            list($serviceTitle, $serviceCost) = explode(':', $service);
            $serviceOrder->setServices($serviceTitle, (float)$serviceCost);
        }

        $serviceOrder->calculateTotalCost();

        $this->prepareDataTable($customer, $vehicle, $serviceOrder, $mechanic);

        $table = new Table($output);
        $table
            ->setHeaders(array_keys($this->dataTable))
            ->addRow(array_values($this->dataTable));
        $table->render();

        return Command::SUCCESS;
    }

    private function prepareDataTable(
        Customer $customer,
        Vehicle $vehicle,
        AutoServiceOrder $serviceOrder,
        Mechanic $mechanic
    ): void {
        $tableData = [
            "Customer's name" => $customer->getFullName(),
            "Customer's phone" => $customer->getPhone(),
            "Vehicle's model" => $vehicle->getModel(),
            "Vehicle's vinCode" => $vehicle->getVinCode(),
            'Service' => implode("\n", array_keys($serviceOrder->getServices())),
            'Cost' => implode("\n", $serviceOrder->getServices()),
            "Total cost" => $serviceOrder->getTotalCost(),
            "Mechanic's name" => $mechanic->getName()
        ];

        $this->dataTable = $tableData;
    }
}