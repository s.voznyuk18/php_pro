<?php

declare(strict_types=1);

namespace CarMaster\Exceptions;

use CarMaster\Classes\Employee;

class EmployeeValidationException extends ValidationException
{
    public function __construct(Employee $obj, string $message)
    {
        $format = "The %s must have $message";
        $message = sprintf($format, get_class($obj));
        parent::__construct($message);
    }
}